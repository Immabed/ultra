﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gvr;

public class AnimateElevator : InteractableObject {

    [SerializeField]
    private Animator anim;
	

    override public void Trigger(string state)
    {
        if (enabled)
        {
            if (state != null && state.Equals("withPlayer"))
            {
                GameObject player = GameManager.Manager.Player;
                player.transform.parent = this.transform;
                player.transform.localPosition = new Vector3(0, 1.4f, -64);
                player.GetComponent<ModifiedFirstPersonController>().enabled = false;
            }
            Debug.Log("Button Pressed");
            anim.SetTrigger("Move");
        }
    }

}
