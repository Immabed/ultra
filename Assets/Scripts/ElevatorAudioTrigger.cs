﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorAudioTrigger : MonoBehaviour
{
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    public void ToggleAudio()
    {
        if (audioSource.isPlaying)
            audioSource.Stop();
        else
            audioSource.Play();
    }

}
