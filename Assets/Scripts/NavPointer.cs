﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavPointer : MonoBehaviour {

    GameObject destination;
	
	// Update is called once per frame
	void Update () {
        if (destination != null)
        {
            transform.LookAt(destination.transform.position, Vector3.up);
        }
	}

    public void SetDestination(GameObject location)
    {
        destination = location;
    }
}
