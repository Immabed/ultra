﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpaceShipSequenceManager : MonoBehaviour {

    static SpaceShipSequenceManager me;
    [SerializeField]
    Animator rocketAnim;
    [SerializeField]
    GameObject rocket;
    [SerializeField]
    GameObject station;
    [SerializeField]
    GameObject solarSystem;
    [SerializeField]
    GameObject mars;
    [SerializeField]
    GameObject interior;
    //[SerializeField]
    //CommsController comms;

    [SerializeField]
    Button bt;
    Text txt;

    [SerializeField]
    AudioClip[] clips;
    [SerializeField]
    AudioSource comms;

    [SerializeField]
    Animator ufoAnim;

    int state = 0;


    public static SpaceShipSequenceManager Manager
    {
        get { return me; }
    }

    // Use this for initialization
    void Start()
    {
        if (me != null && this != me)
        {
            Destroy(this);
        }
        else { me = this; }
        txt = bt.GetComponentInChildren<Text>();
        bt.interactable = false;
        txt.text = "";

        StartSequence();
    }

    void StartSequence()
    {
        comms.clip = clips[0];
        comms.Play();
        state = 1;
        StartCoroutine(PreLaunchSequence());
        
    }

    public void ButtonPressed()
    {
        switch (state) {
            case 1: LaunchRocket(); break;
            case 3: Rendesvous(); break;
            case 5: EmergencyStop(); break;
            case 7:
                mars.SetActive(false);
                //Change scene
                SceneManager.LoadScene("mars");
                break;
            default: Debug.Log("Improper state during button press"); break;
        }
    }

    void LaunchRocket()
    {
        state = 2;
        bt.interactable = false;
        txt.text = "";
        StartCoroutine(RocketSequence());
    }

    void Rendesvous()
    {
        state = 4;
        bt.interactable = false;
        txt.text = "";
        StartCoroutine(StationSequence());
    }

    void EmergencyStop()
    {
        state = 6;
        bt.interactable = false;
        txt.text = "";
        StartCoroutine(SolarSystemSequence());
    }

    //Pre Launch
    IEnumerator PreLaunchSequence()
    {
        yield return new WaitForSeconds(20);
        comms.clip = clips[1];
        comms.Play();
        yield return new WaitForSeconds(8);
        comms.clip = clips[2];
        comms.Play();

        yield return new WaitForSeconds(3);
        state = 1;
        bt.interactable = true;
        txt.text = "Launch";
    }

    // Rocket Launch
    IEnumerator RocketSequence()
    {
        rocket.SetActive(true);
        RocketEventController rc = rocket.GetComponent<RocketEventController>();

        // Ignition
        yield return new WaitForSeconds(10);
        comms.clip = clips[3];
        comms.Play();
        yield return new WaitForSeconds(5);
        rc.StartStage1Thrust();

        yield return new WaitForSeconds(3);
        comms.clip = clips[4];
        comms.Play();

        // MECO
        yield return new WaitForSeconds(20);
        rocketAnim.SetTrigger("Staging");
        yield return new WaitForSeconds(2);
        comms.clip = clips[5];
        comms.Play();

        // Upper Burn
        yield return new WaitForSeconds(25);
        comms.clip = clips[6];
        comms.Play();

        // SECO
        yield return new WaitForSeconds(10);
        rocketAnim.SetTrigger("Staging");
        yield return new WaitForSeconds(5);
        comms.clip = clips[7];
        comms.Play();
        

        // End sequence
        yield return new WaitForSeconds(10);
        rocket.SetActive(false);
        bt.interactable = true;
        txt.text = "Rendesvous";
        state = 3;
    }

    // Space Station
    IEnumerator StationSequence()
    {
        station.SetActive(true);

        yield return new WaitForSeconds(4);

        // rotate into view
        float t = 0;
        float time = 4;
        while (t <= 4)
        {
            float delta = Time.deltaTime * 100 / time;
            t += Time.deltaTime;
            interior.transform.Rotate(new Vector3(0, delta, 0));
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(5);

        // Call animations
        ufoAnim.SetTrigger("Activate");

        yield return new WaitForSeconds(3);
        comms.clip = clips[8];
        comms.Play();

        // End sequence
        yield return new WaitForSeconds(8);
        bt.interactable = true;
        txt.text = "EMERGENCY STOP";
        state = 5;
        station.SetActive(false);
    }

    IEnumerator SolarSystemSequence()
    {
        solarSystem.SetActive(true);
        comms.clip = clips[9];
        comms.Play();
        yield return new WaitForSeconds(18);
        solarSystem.SetActive(false);
        mars.SetActive(true);
        comms.clip = clips[10];
        comms.Play();
        yield return new WaitForSeconds(10);

        bt.interactable = true;
        state = 7;
        txt.text = "Go To Mars";
    }
}
