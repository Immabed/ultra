﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketEventController : MonoBehaviour {

    [SerializeField]
    private ParticleSystem stage1particles;
    [SerializeField]
    private ParticleSystem stage2particles;
    [SerializeField]
    private GameObject stage1;
    [SerializeField]
    private GameObject stage2;
    [SerializeField]
    private GameObject fairings;
    [SerializeField]
    private GameObject rocket;
    [SerializeField]
    private Animator fairingAnimator;
    [SerializeField]
    RocketSoundController rocketAudio;


    public void StartStage1Thrust()
    {
        ParticleSystem.EmissionModule em = stage1particles.emission;
        em.enabled = true;
        rocketAudio.Both();
    }

    public void StopStage1Thrust()
    {
        ParticleSystem.EmissionModule em = stage1particles.emission;
        em.enabled = false;
        rocketAudio.Stop();
    }

    public void StartStage2Thrust()
    {
        ParticleSystem.EmissionModule em = stage2particles.emission;
        em.enabled = true;
        rocketAudio.LowOnly();
    }

    public void StopStage2Thrust()
    {
        ParticleSystem.EmissionModule em = stage2particles.emission;
        em.enabled = false;
        fairingAnimator.SetTrigger("Staging");
        rocketAudio.Stop();
    }

    public void HideStage1()
    {
        stage1.SetActive(false);
    }

    public void HideStage2()
    {
        stage2.SetActive(false);
        fairings.SetActive(false);
        fairingAnimator.gameObject.SetActive(false);
    }

    public void ZoomLevel1()
    {
        Vector3 endPos = new Vector3(0.76f, -0.34f, 0);
        Vector3 endScale = new Vector3(0.03f, 0.03f, 0.03f);
        Vector3 startPos = rocket.transform.localPosition;
        Vector3 startScale = rocket.transform.localScale;
        StartCoroutine(LerpTransform(startScale, endScale, startPos, endPos, 2));
    }

    public void ZoomLevel2()
    {
        Vector3 endPos = new Vector3(1.47f, -1.23f, 0);
        Vector3 endScale = new Vector3(0.05f, 0.05f, 0.05f);
        Vector3 startPos = rocket.transform.localPosition;
        Vector3 startScale = rocket.transform.localScale;
        StartCoroutine(LerpTransform(startScale, endScale, startPos, endPos, 2));
    }

    private IEnumerator LerpTransform(Vector3 startScale, Vector3 endScale, Vector3 startPos, Vector3 endPos, float time)
    {
        float t = 0;
        while (t / time <= 1)
        {
            rocket.transform.localPosition = Vector3.Lerp(startPos, endPos, t / time);
            rocket.transform.localScale = Vector3.Lerp(startScale, endScale, t / time);
            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
