﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketSoundController : MonoBehaviour {

    [SerializeField]
    AudioSource lowRumble;
    [SerializeField]
    AudioSource regularRumble;

	public void Stop()
    {
        lowRumble.Stop();
        regularRumble.Stop();
    }

    public void Both()
    {
        lowRumble.Play();
        regularRumble.Play();
    }

    public void LowOnly()
    {
        lowRumble.Play();
        regularRumble.Stop();
    }
}
