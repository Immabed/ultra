﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static GameManager me;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private LaunchComplexAudioPlayer lcComplex;

    public LaunchComplexAudioPlayer LC { get { return lcComplex; } }

    public static GameManager Manager
    {
        get { return me; }
    }
    
    public GameObject Player { get { return player; } }

    public bool CanJump { get; set; }

    // Use this for initialization
    void Start()
    {
        if (me != null && this != me)
        {
            Destroy(this);
        }
        else { me = this; }

        CanJump = true;
    }
}
