﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitElevator : StateMachineBehaviour {


	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	//override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//    
	//}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (GameManager.Manager.Player.transform.parent == animator.transform)
        {
            GameObject player = GameManager.Manager.Player;
            player.transform.parent = null;
            Debug.Log(player.transform.position);
            player.transform.Translate(new Vector3(0, 1, -7), Space.World);
            Debug.Log(player.transform.position);
            player.GetComponent<ModifiedFirstPersonController>().enabled = true;
            GameManager.Manager.LC.EnterBridge();
        }
        animator.ResetTrigger("Move");
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
