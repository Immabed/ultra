﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLaunchController : MonoBehaviour {


    [SerializeField]
    ParticleSystem exhaust;
    [SerializeField]
    AudioSource exhaustSound;

    bool isLaunching = false;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (isLaunching)
        {
            rb.AddForce(new Vector3(0, 4, 0), ForceMode.Acceleration);
        }
        if (transform.position.y >= 5000)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Launch()
    {
        ParticleSystem.EmissionModule em = exhaust.emission;
        em.enabled = true;
        exhaustSound.Play();
        isLaunching = true;
    }
}
