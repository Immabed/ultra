﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchComplexAudioPlayer : MonoBehaviour {

    [SerializeField]
    AudioSource audioPlayer;

    [SerializeField]
    RocketLaunchController lc;

    [SerializeField]
    NavPointer navPointer;

    [SerializeField]
    GameObject elevatorTransform;
    [SerializeField]
    GameObject spaceShipTransform;

    [SerializeField]
    AudioClip C1;
    [SerializeField]
    AudioClip C2;
    [SerializeField]
    AudioClip C3;
    [SerializeField]
    AudioClip C4;

    bool c4Played = false;

	// Use this for initialization
	void Start () {
        navPointer.SetDestination(elevatorTransform);
        audioPlayer.clip = C1;
        audioPlayer.Play();
        StartCoroutine(WaitForLaunch());
	}

    IEnumerator WaitForLaunch()
    {
        yield return new WaitForSeconds(12.5f);
        audioPlayer.clip = C2;
        audioPlayer.Play();

        yield return new WaitForSeconds(10);
        lc.Launch();

        yield return new WaitForSeconds(40);

        audioPlayer.clip = C3;
        audioPlayer.Play();
    }

    public void EnterBridge()
    {
        if (!c4Played)
        {
            c4Played = true;
            audioPlayer.clip = C4;
            audioPlayer.Play();
            navPointer.SetDestination(spaceShipTransform);
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
