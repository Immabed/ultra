﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommsController : MonoBehaviour {

    [SerializeField]
    AudioSource comm;

    public void Countdown()
    {
        comm.Play();
    }
}
