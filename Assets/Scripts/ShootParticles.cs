﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootParticles : MonoBehaviour {

    [SerializeField]
    GameObject laser;
    [SerializeField]
    GameObject explosion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShootLaser()
    {
        laser.SetActive(true);
        StartCoroutine(Explosion());
    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(0.5f);
        explosion.SetActive(true);
    }
}
