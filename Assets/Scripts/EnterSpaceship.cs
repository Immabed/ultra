﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterSpaceship : InteractableObject {

    public override void Trigger(string state)
    {
        // load Scene
        SceneManager.LoadScene("interior");
        Debug.Log("Clicked on Spaceship.");
    }

    
}
