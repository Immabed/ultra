﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObject : MonoBehaviour {

    private bool triggerEnabled;
    [SerializeField]
    MeshRenderer indicator;
    [SerializeField]
    Material interactable;
    [SerializeField]
    Material interacting;

    public bool enableSelection
    {
        get { return triggerEnabled; }
        set {
            GameManager.Manager.CanJump = !value;
            if (indicator != null)
                indicator.material = value ? interacting : interactable;
            triggerEnabled = value;
        }
    }

    public abstract void Trigger(string state);
}
